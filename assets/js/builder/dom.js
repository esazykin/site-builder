'use strict';

angular.module('builder')

    .factory('dom', ['$rootScope', 'undoManager', 'elements', 'css', function ($rootScope, undoManager, elements, css) {

        var dom = {

            copiedNode: null,

            /**
             * Html page meta data.
             *
             * @type {Object}
             */
            meta: {},

            /**
             * Move selected node by one element in the specified direction.
             *
             * @param  string  dir
             * @param  DOM     node
             *
             * @return void
             */
            moveSelected: function (dir, node) {
                if (!node) node = $rootScope.selected.parent;

                if (dir == 'down') {
                    //check if there's an element after this one
                    var next = $($rootScope.selected.node).next();

                    if (next[0]) {

                        //check if we can insert selected node into the next one
                        if (elements.canInsertSelectedTo(next[0])) {
                            next.prepend($rootScope.selected.node);
                        } else {
                            next.after($rootScope.selected.node);
                        }

                    } else {
                        var parentParent = $($rootScope.selected.node).parent().parent();

                        if (elements.canInsertSelectedTo(parentParent[0])) {
                            $($rootScope.selected.node).parent().after($rootScope.selected.node);
                        }
                    }

                    $rootScope.repositionBox('select');
                }

                else if (dir == 'up') {
                    //check if there's an element before this one
                    var prev = $($rootScope.selected.node).prev();

                    if (prev[0]) {

                        //check if we can insert selected node into the prev one
                        if (elements.canInsertSelectedTo(prev[0])) {
                            prev.append($rootScope.selected.node);
                        } else {
                            prev.before($rootScope.selected.node);
                        }

                    } else {
                        var parentParent = $($rootScope.selected.node).parent().parent();

                        if (elements.canInsertSelectedTo(parentParent[0])) {
                            $($rootScope.selected.node).parent().before($rootScope.selected.node);
                        }
                    }

                    return $rootScope.repositionBox('select');
                }
            },

            /**
             * Append element user is currently dragging to the element users cursor is under.
             *
             * @param  DOM node
             * @return void
             */
            appendSelectedTo: function (node, point) {
                if (!node) return true;

                //check if we're not trying to drop a node inside it's child or itself
                if ($rootScope.selected.node == node || $rootScope.selected.node.contains(node)) {
                    return true;
                }

                //get all the children of passed in node
                //todo might need to remove scope.active.node
                var contents = node.children, n;

                for (var i = 0, len = contents.length; i < len; i++) {
                    n = contents[i];

                    //check if users cursor is currently above any of the given nodes children
                    if (this.above(n, point)) {

                        //if we can insert active element to given node and users
                        //cursor is above one of the children of that node then insert
                        //active element before that child and bail
                        if (elements.canInsertSelectedTo(node)) {

                            //make sure we don't insert elements before body node
                            if (n.nodeName == 'BODY') {
                                n.appendChild($rootScope.selected.node);
                            } else {
                                n.parentNode.insertBefore($rootScope.selected.node, n);
                            }

                            //reposition context boxes
                            return $rootScope.repositionBox('select');
                        }
                    }
                }

                //if users cursor is not above any children on the node we'll
                //just append active element to the node
                if (elements.canInsertSelectedTo(node)) {
                    node.appendChild($rootScope.selected.node);
                }

                return true;
            },

            /**
             * Return whether or not given coordinates are above given element in the dom.
             *
             * @param  DOM el
             * @param  Object point {x, y}
             *
             * @return boolean
             */
            above: function (el, point) {

                if (el.nodeName !== '#text') {
                    var offset = el.getBoundingClientRect();
                    var width = el.offsetWidth;
                    var height = el.offsetHeight;

                    var box = [
                        [offset.left, offset.top], //top left
                        [offset.left + width, offset.top], //top right
                        [offset.left + width, offset.top + height], //bottom right
                        [offset.left, offset.top + height] //bottom left
                    ];

                    var beforePointY = box[0][1];
                    var beforePointX = box[0][0];

                    if (point.y < box[2][1]) {
                        return point.y < beforePointY || point.x < beforePointX
                    }

                    return false
                }
            },

            /**
             * Replace builder iframe html with given one.
             *
             * @param html string
             * @return void
             */
            loadHtml: function (html, loadHead) {
                if (!html) {
                    $rootScope.frameBody.html('');
                    return;
                }

                var doc = new DOMParser().parseFromString(html, "text/html");

                if (loadHead) {
                    $rootScope.frameHead.html($rootScope.frameHead.html() + doc.head.innerHTML);
                }
                Array.from(doc.body.parentNode.attributes)
                    .forEach(function(attr) {
                        return $rootScope.frameDoc.body.parentNode.setAttribute(attr.nodeName, attr.nodeValue)
                    });
                Array.from(doc.body.attributes)
                    .forEach(function (attr) {
                        return $rootScope.frameDoc.body.setAttribute(attr.nodeName, attr.nodeValue)
                    });

                $rootScope.frameBody[0].innerHTML = doc.body.innerHTML;
                    // $rootScope.frameBody.html(doc.body.innerHTML);
            },

            /**
             * Return compiled html for currently active or passed in page.
             *
             * @param  object|void   page        page to compile html for
             * @param  boolean       includeCss  Whether or not all css should be included in compiled html
             * @param  boolean       includeJs   Whether or not all js should be included in compiled html
             * @param  mixed         pageForJs   page that only js assets will be fetched from
             *
             * @return string
             */
            getHtml: function (page, includeCss, includeJs, pageForJs) {

                // debugger;

                var head = $rootScope.frameHead[0].outerHTML;
                var doc = new DOMParser().parseFromString(head, "text/html");
                if (includeCss) {
                    var x = css.compile();
                    $(doc.head).append("<style>" + x + "</style>");
                } else {
                    $("#editor-css, #elements-css, #inspector-css, #injected-script-form-proxy", doc).remove();
                }
                head = doc.head.outerHTML;


                var body = $rootScope.frameBody[0].outerHTML.replace('contenteditable="true"', '');

                var rootAttrs = ' ';
                if ($rootScope.frameDoc.body.parentNode.attributes.length) {
                    rootAttrs = Array.from($rootScope.frameDoc.body.parentNode.attributes)
                        .reduce(function (prev, cur) {
                            return prev.concat(' ' + cur.nodeName + '="' + cur.nodeValue + '"')
                        }, '');
                }
                //compile everything into complete html string
                return "<!DOCTYPE html>\n<html" + rootAttrs + ">" + head + body + "</html>";
            },

            /**
             * Converts preview nodes html to their actual html (iframe image to actual iframe) in the given string.
             *
             * @param html
             * @returns {string}
             */
            previewsToHtml: function (html) {
                return html.replace(/<img.+?data-src="(.+?)".+?>/gi, '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="$1"></iframe></div>');
            },

            /**
             * Set meta data for currently active page.
             *
             * @param object meta
             */
            setMeta: function (meta) {
                this.meta = meta;
            },

            /**
             * Copy given node for later use or pasting.
             *
             * @param  DOM   node
             * @return void
             */
            copy: function (node) {
                if (node && node.nodeName != 'BODY') {
                    this.copiedNode = $(node).clone();
                }
            },

            /**
             * Paste copied DOM node if it exists.
             *
             * @param  DOM node
             * @return void
             */
            paste: function (node) {
                if (node && this.copiedNode) {

                    //make sure we don't paste nodes after body
                    if (node.nodeName == 'BODY') {
                        $(node).append(this.copiedNode);
                    } else {
                        $(node).after(this.copiedNode);
                    }

                    //register a new undo command with undo manager
                    var params = {node: this.copiedNode};
                    params.parent = params.node.parent();
                    params.parentContents = params.parent.contents();
                    params.undoIndex = params.parentContents.index(params.node.get(0));
                    undoManager.add('insertNode', params);

                    this.copiedNode = null;

                    $rootScope.$broadcast('builder.html.changed');
                }
            },

            /**
             * Copy and remove the given node.
             *
             * @param  DOM node
             * @return void
             */
            cut: function (node) {
                if (node && node.nodeName != 'BODY') {
                    this.copy(node);
                    this.delete(node);
                }
            },

            /**
             * Delete a node from the DOM.
             *
             * @param  DOM   node
             * @return void
             */
            delete: function (node) {

                if (node && node.nodeName != 'BODY') {

                    if (node.parentNode) {
                        $rootScope.$apply(function () {
                            $rootScope.selectNode(node.parentNode);
                        });
                    }

                    //register a new undo command with undoManager before we remove the node
                    var params = {node: node};
                    params.parent = params.node.parentNode;
                    params.parentContents = params.parent.childNodes;
                    params.undoIndex = utils.getNodeIndex(params.parentContents, params.node);
                    undoManager.add('deleteNode', params);

                    $(node).remove();

                    $rootScope.hoverBox.hide();
                    $rootScope.repositionBox('select');

                    $rootScope.$broadcast('builder.html.changed');
                }
            },

            clone: function (node) {
                this.copy(node);
                this.paste(node);
            },

            wrapInTransparentDiv: function (node) {
                node = $(node);

                //insert given node contents into the transparent wrapper
                var wrapper = $(
                    '<div class="transparent-background" style="background-color: rgba(0,0,0,0.8)"></div>'
                ).append(node.contents());

                //append wrapper to node and get nodes padding
                var padding = node.append(wrapper).css('padding');

                //move padding from node to wrapper to avoid white space
                wrapper.css('padding', padding);
                node.css('padding', 0);

                $rootScope.$broadcast('builder.html.changed');
            },

            //tables

            /**
             * Add a new row to the table before given node.
             *
             * @type void
             */
            addRowBefore: function (node) {
                var tr = $(node).closest('tr');

                tr.before(tr.clone());

                $rootScope.repositionBox('select');
            },

            /**
             * Add a new row to the table after given node.
             *
             * @type void
             */
            addRowAfter: function (node) {
                var tr = $(node).closest('tr');

                tr.after(tr.clone());

                $rootScope.repositionBox('select');
            },

            addColumnAfter: function (node) {
                this.addColumn(node, 'after');
            },

            addColumnBefore: function (node) {
                this.addColumn(node, 'before');
            },

            /**
             * Add a new column to the table.
             *
             * @type void
             */
            addColumn: function (node, direction) {
                if (node.nodeName !== 'TD') {
                    return false
                }
                ;

                var node = $(node),
                    index = node.index(),
                    table = node.closest('table');

                table.find('thead tr').children('th').each(function (i, v) {
                    if (i === index) {
                        $(v)[direction]($(v).clone());
                        return false;
                    }
                });

                table.find('tbody tr').each(function (i, v) {
                    $(v).children('td').each(function (i, v) {
                        if (i === index) {
                            $(v)[direction]($(v).clone());
                            return false;
                        }
                    });
                });

                $rootScope.repositionBox('select');
            }
        };

        return dom;

    }]);