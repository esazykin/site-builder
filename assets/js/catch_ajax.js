var ajaxListener = {};
ajaxListener.nativeSend = XMLHttpRequest.prototype.send;
XMLHttpRequest.prototype.send = function (data) {
    function deparam(a) {
        var b, c, d, e, f, g = {};
        if ("?" === a.slice(0, 1) && (a = a.slice(1)), "" !== a)for (b = a.split("&"), c = 0; c < b.length; c += 1)d = b[c].split("="), e = decodeURIComponent(d[0]), f = d.length > 1 ? decodeURIComponent(d[1]) : void 0, g[e] = f;
        return g
    }
    var opts = {
        "": "",
        name: "ФИО",
        gender: "Пол",
        country_id: "Страна",
        city_id: "Город",
        birthday: "Дата рождения",
        email: "Email",
        phone: "Телефон"
    };
    function getSelect(postedField) {
        console.log(window.parent.field2field);
        var preselect = false;
        var select = "";
        if (window.parent.field2field && window.parent.field2field[postedField]) {
            preselect = window.parent.field2field[postedField];
        }
        for (var i in opts) {
            var selected = '';
            if (preselect == i) {
                selected = 'selected';
            }
            select += "<option value='" + i + "' " + selected + ">" + opts[i] + "</option>";
        }
        return "<select class='select-data-name'>" + select + "</select>";
    }

    data = deparam(data);
    if ($("#params_link").length) {
        var box = $("#params_link")
    }
    else {
        var box = $("<div id='params_link'></div>").appendTo(document.body);
    }
    var html = "";
    for (var i in data)
        html += "<div data-field='" + i + "'><b>" + i + "</b> -> <span>" + data[i] + "</span>" + getSelect(i) + "</div>";
    box.html(html + "<br/><button id='save_params_link'>Сохранить</button>");
    $("body").off("click", "#save_params_link").on("click", "#save_params_link", function () {
        var data = [];
        console.log(window.parent.field2field);
        $(".select-data-name").each(function (i) {
            window.parent.field2field[this.parentNode.dataset.field] = this.value;
            data.push("fields[" + i + "][field_from]=" + this.parentNode.dataset.field + "&fields[" + i + "][field_to]=" + this.value);
        });
        console.log(window.parent.field2field);
        $('<img src="/field2field?id_project=' + projectId + "&" + data.join('&') + '"/>');
    });
};
