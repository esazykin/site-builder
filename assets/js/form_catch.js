
document.addEventListener("DOMContentLoaded", function () {
    var forms = [];
    for (var i = 0; i < document.forms.length; i++) {
        if (typeof document.forms[i].getAttribute("action") == "string") {
            forms.push(document.forms[i])
        }
    }

    console.log(forms);

    forms.forEach(function (elem) {
        elem.addEventListener("submit", function (event) {
            var form = event.target;
            var formData  = new FormData(form);
            var postData = [];
            for (var pair of formData.entries()) {
                postData.push(pair[0] + "=" + encodeURIComponent(pair[1]));
            }
            console.log(postData);

            var xmlHttpRequest = new XMLHttpRequest();
            xmlHttpRequest.open(form.method, form.getAttribute('action'));
            xmlHttpRequest.send(postData.join('&'));

            event.preventDefault();
        });
    });
});