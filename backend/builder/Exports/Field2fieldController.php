<?php

namespace Builder\Exports;

use Builder\Projects\ProjectModel;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Database\Capsule\Manager as Capsule;

class Field2fieldController
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Application
     */
    private $app;

    public function __construct(Request $request, $app)
    {
        $this->request = $request;
        $this->app = $app;
    }

    public function index()
    {
        $id = $this->request->get('id_project');
        $project = ProjectModel::find($id);
        $fields = $this->request->get('fields', []);

        $capsule = $this->app['illuminate.capsule'];
        $conf = require $this->app['base_dir'] . '/backend/config/db2.php';
        $capsule->addConnection($conf, 'trafficsland');

        $existsFields = Capsule::table('site_forms', 'trafficsland')->where('tasks_domain_id', '=', $project->tasks_domain_id)->get();
        $existsFields = collect($existsFields)->keyBy('field_from');
        foreach ($fields as $field) {
            $isOrigin = strpos($field['field_from'], "tmp_tmp_tmp_tmp_tmp_") === false;
            if ($existsFields->has($field['field_from'])) {
                Capsule::table('site_forms', 'trafficsland')->where('id', '=', $existsFields[$field['field_from']]['id'])
                    ->update([
                        'field_to' => $field['field_to'],
                        'is_origin' => $isOrigin
                    ]);
            } else {
                Capsule::table('site_forms', 'trafficsland')->insert([
                    'tasks_domain_id' => $project->tasks_domain_id,
                    'field_from' => $field['field_from'],
                    'field_to' => $field['field_to'],
                    'is_origin' => $isOrigin
                ]);
            }
        }


        $existsFields = Capsule::table('site_forms', 'trafficsland')->where('tasks_domain_id', '=', $project->tasks_domain_id)->get();
        $r = [];
        foreach ($existsFields as $v) {
            $r[$v['field_from']] = $v['field_to'];
        }

        return new JsonResponse($r, 200);
    }

    public function get()
    {
        $id = $this->request->get('projectId');
        $project = ProjectModel::find($id);

        $capsule = $this->app['illuminate.capsule'];
        $conf = require $this->app['base_dir'] . '/backend/config/db2.php';
        $capsule->addConnection($conf, 'trafficsland');

        $existsFields = Capsule::table('site_forms', 'trafficsland')->where('tasks_domain_id', '=', $project->tasks_domain_id)->get();
        $r = [];
        foreach ($existsFields as $v) {
            $r[$v['field_from']] = $v['field_to'];
        }

        return new JsonResponse($r, 200);
    }
}
