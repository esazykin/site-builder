<?php

namespace Builder\Import;

use Builder\Projects\PageModel;
use Builder\Projects\ProjectModel;
use Builder\Users\UserModel;
use Silex\Application;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ImportController
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @var Application
     */
    private $app;

    public function __construct(Request $request, Filesystem $fs, $app)
    {
        $this->request = $request;
        $this->fs = $fs;
        $this->app = $app;
    }

    public function template()
    {
        $projectInfo = $this->request->get('project', []);
        $htmlFiles = $this->request->get('html', []);
        $project = ProjectModel::create($projectInfo);
        foreach ($htmlFiles as $htmlFile) {
            $htmlFile['source_html'] = $htmlFile['html'];
            $page = new PageModel($htmlFile);
            $project->pages()->save($page);
            $user = UserModel::find(1);
            $user->projects()->attach($project->id);
        }

        $this->fs->copy(
            "http://$projectInfo[hostname]/$projectInfo[screen]",
            $this->app['base_dir'] . "/assets/images/projects/project-$project->id.jpeg"
        );

        return new JsonResponse(['success' => 1], 200);
    }
}
